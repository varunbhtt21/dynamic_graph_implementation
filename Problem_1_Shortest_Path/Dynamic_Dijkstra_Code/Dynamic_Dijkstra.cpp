#include<stdio.h>
#include<iostream>
#include<math.h>
#include<bits/stdc++.h>
#include <math.h>
#include <stdio.h>
#include<bits/stdc++.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <fstream>
#include <time.h>
#include <stdbool.h>
using namespace std;


#define parent(i) (i/2)
#define heap_left(i) (2*i)
#define heap_right(i) (2*i+1)
#define MAX 500000                      // 0xFFFFFFFF



// GLOBAL VARIABLES

typedef pair<int, int> iPair; 
vector<iPair > adj[50000]; 
vector<int> SP;





void heapify(int A[][2], int i, int n){

	int left = heap_left(i);
	int right = heap_right(i);
	int low = 0;


	if( left <= n && A[left-1][0] < A[i-1][0] ){
		low = left;
	}
	else {
		low = i;
	}

	if( right <= n && A[right-1][0] < A[low-1][0]){
		low = right;
	}

	if( low != i ){
		int t1 = A[i-1][0];
		int t2 = A[i-1][1];

		A[i-1][0] = A[low-1][0];
		A[i-1][1] = A[low-1][1];
		A[low-1][0] = t1;
		A[low-1][1] = t2;

		heapify(A, low, n);

	}
}








void BUILD_MIN_HEAP(int A[][2], int n){

	for(int i = (n/2); i>0; i--){
		heapify(A, i, n);
	}
}








int MinHeap_ExtractMin(int A[][2], int n, int *Index_Node){

	if( n < 1 ){
		printf("Underflow\n");
		return -1;
	}

	int min_weight = A[0][0];
	int min_node = A[0][1];
	


	if(n > 1){
		A[0][0] = A[n-1][0];
		A[0][1] = A[n-1][1];

		BUILD_MIN_HEAP(A, n-1);

	}
	*Index_Node = min_node;

 
	return min_weight;
}










void decrease_key(int heap[][2], int u, int weight, int n){

	for(int i=0; i<n ; i++){

		if(heap[i][1] == u){   // IInd value is node

			if( weight < heap[i][0] ) {  // Heap Ist value is weight

				heap[i][0] = weight;
				int parent = parent(i);

				while( i > 0 && heap[parent][0] > heap[i][0] ){

					int t1, t2;


					//SWAP
					t1 = heap[parent][0];
					t2 = heap[parent][1];

					heap[parent][0] = heap[i][0];
					heap[parent][1] = heap[i][1];

					heap[i][0] = t1;
					heap[i][1] = t2;



					i = parent;
					parent = parent(i);

				}

			}
			break;
		}
	}

}











void relax(int heap[][2], int distance[][2], int u, int v, int w, int n, int t){

	int weight = distance[u][0] + w;
	
	if(t==54){
	  //  cout<<"dekh le u v pr "<<u<<" "<<v<<" "<<w;
	   // cout<<"d[u][0] "<<distance[u][0]<<" d[v][0] "<<distance[v][0];
	}
	
	

	if( distance[v][0] > weight ){
		distance[v][0] = weight;  // Ist index is weight
		distance[v][1] = u;		  // IInd index is to which node it connected
		
		decrease_key(heap, v, weight, n);
				// decrease the value of v node by weight w
				// n is total nodes
	}	

}









void Dijkstra_for_Deletion(int Adj_Matrix[][2000], int distance[][2], int u,int v, int nodes){

	int heap[nodes][2];
	int Visited[nodes];
	int index;

	for(int i = 0; i<SP.size(); i++){

		if( SP[i] == u ){

			index = i;
		}

		heap[i][0] = MAX;   // First Value is Weights
		heap[i][1] = i;		// Second Value is Nodes
		
	}

	for(int i = 0; i<=index; i++){


		heap[i][0] = MAX;   // First Value is Weights
		heap[i][1] = i;		// Second Value is Nodes
		
	}




	for(int i = index+1; i<SP.size(); i++){
			
	    distance[SP[i]][0] = MAX;
		distance[SP[i]][1] = MAX;

		Visited[i] = 0;     // Visited Nodes
		
	}

	heap[SP[index]][0] = distance[SP[index]][0];

	int heap_size = nodes-index;


	BUILD_MIN_HEAP(heap, heap_size);

	while(heap_size > 0){
		int node_pos = -1;
		int min_weight = MinHeap_ExtractMin(heap, heap_size, &node_pos);

	//	cout<<"min_weight "<<min_weight<<" "<<node_pos;

		//cout<<"Shortest Path "<<node_pos;


		if(node_pos == -1){
			printf("Got Wrong \n");
			break;
		}

		Visited[node_pos] = 1;
		heap_size--;        // We have extracted the min



		if(min_weight == -1){
			break;
		}

		else{

					for( int vertex = 0; vertex < nodes; vertex++ ){
					   
						int weight = Adj_Matrix[node_pos][vertex];

						if(weight != -1 && Visited[vertex] != 1){
					
							relax(heap, distance, node_pos, vertex, weight, heap_size,54);
						}
					}

		    }// END OF ELSE
	 }



 }



void Dijkstra_Shortest_Path(int Adj_Matrix[][2000], int distance[][2], int start, int nodes)
{
	int heap[nodes][2];
	int Visited[nodes];

	for(int i = 0; i<nodes; i++){
		distance[i][0] = MAX;
		distance[i][1] = MAX;

		heap[i][0] = MAX;   // First Value is Weights
		heap[i][1] = i;		// Second Value is Nodes

		Visited[i] = 0;     // Visited Nodes
	}

	distance[start][0] = 0;  // Initial Node Wt is 0
	distance[start][1] = 0;	 // Initial Node is 0

	heap[start][0] = 0;

	int heap_size = nodes;

	BUILD_MIN_HEAP(heap, heap_size);

	while(heap_size > 0){
		int node_pos = -1;
		int min_weight = MinHeap_ExtractMin(heap, heap_size, &node_pos);

		
		SP.push_back(node_pos);        // Keeping track of path 


		if(node_pos == -1){
			printf("Got Wrong \n");
			break;
		}

		Visited[node_pos] = 1;
		heap_size--;        // We have extracted the min



		if(min_weight == -1){
			break;
		}

		else{

					for( int vertex = 0; vertex < nodes; vertex++ ){
						int weight = Adj_Matrix[node_pos][vertex];

						if(weight != -1 && Visited[vertex] != 1){
							relax(heap, distance, node_pos, vertex, weight, heap_size,0);
						}
					}

		    }// END OF ELSE
	 }
}
















void myswap(int *a, int *b){
	int temp=0;

	temp = *a;
	*a = *b;
	*b = temp;
}

















void heap_insertion( int heap_dyn[][2], int weight, int index ,int *size_dyn){
	int parent;


	*size_dyn=*size_dyn + 1; // starts from 1
	int i = *size_dyn - 1;


		heap_dyn[0][0] = weight;
		heap_dyn[0][1] = index;

}













 void insertEdge(int u, int v, int w, int t, int d[][2]){  
     
     
    // FIRST PART

	int heap[t+1][2]; 
	                                      //(u,v) is edge
	memset(heap, MAX, sizeof(heap));						  // d is the shortest distance calculated by dijkstra
	int size = 0;

	
	int mindist = d[u][0] + w;
	

	if( d[v][0] > mindist ){
		d[v][0] = mindist;
		
		heap_insertion(heap, d[v][0], v, &size); // d[v][1] gives to which node i am connected
	}
	
	
	
	
	// SECOND PART
	


	while( size != 0 ){
	    	
	    int max , heap_var;
	    max = 50000;
		int index = -1;

		MinHeap_ExtractMin(heap, size, &index);
		size--;


		

		// THIRD PART

		int u = index; // I need vertex name

				if( d[u][0] < d[t][0] ){
				    
					for(auto x : adj[u]){

						int v = x.first; 
            			int weight = x.second; 

            						if(weight + d[u][0] < d[v][0]){
            							d[v][0] = weight + d[u][0];
            							
            							
            							if( max > x.second ){
            
            							max = x.second;  // weight
            							heap_var = x.first;  // vertex
            
            					    	}
            							
            				    	}// END OF IF

				   	}// END OF FOR
				   	
				}  // END OF IF


				
				if(max != 50000){
				heap_insertion(heap, d[v][0], v, &size);
				}

	}

}











int main(){

			clock_t t; 
	    	t = clock(); 

			int testcases;


			ofstream myfile;
  			myfile.open ("TestCases_Results.txt", std::ios_base::app);
			
  			// Reading Test Cases
			char file_name[100];
			cout<<"Give the name of testcase file : ";
			scanf("%s",file_name);

			// Reading Test Cases
			freopen(file_name, "r", stdin);

			// scanf("%d", &testcases);

			testcases = 1;


			for( int i = 0 ; i < testcases ; i++ ){

				int nodes;
				int edges;

				scanf("%d", &nodes);
				scanf("%d", &edges);

			//	cout<<"Nodes are shuru mai "<<nodes<<" "<<edges;
			
			int Adj_Matrix[nodes][2000];
			int Shortest_Dist[nodes][2];

			memset(Adj_Matrix, -1, sizeof(Adj_Matrix));
			memset(Shortest_Dist, -1, sizeof(Shortest_Dist));

			
			for( int i = 0; i < edges/2; i++ ){
				int x;
				int y;
				int weight;


				scanf("%d", &x);
				scanf("%d", &y);
				scanf("%d", &weight);

				adj[x].push_back(make_pair(y, weight)); 

	
					Adj_Matrix[x][y] = weight; 
				
			}


	    	


				int start = 10;
			
				Dijkstra_Shortest_Path(Adj_Matrix, Shortest_Dist, start, nodes);
			
			 	int a, b ,c;

				 for(int i=0;i<edges/2;i++){

				 	cin>>a;
					cin>>b;
					cin>>c;

					insertEdge(a, b, c, nodes, Shortest_Dist);

			 	}
			

			
			 t = clock() - t; 
    		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds 

    
 	 
 	 		
    		 myfile<<"Nodes : "<<nodes<<"   Edges : "<<edges<<"   Time : "<<time_taken*1000<<" miliseconds";
             myfile << "\n";


			for(int i = 0; i<nodes; i++){

				if(Shortest_Dist[i][0] == 50000){

					Shortest_Dist[i][0] = -1;

				}

				printf("node: %d  weight: %d\n", i+1, Shortest_Dist[i][0]);
			}
			cout<<"Nodes are "<<nodes<<" "<<edges;
			cout<<"\n Time taken is "<<time_taken*1000<<" miliseconds";
			cout<<"\n\n File updated TestCases_Results.txt \n\n";
			
		}
		printf("\n");



   return 0;
}





